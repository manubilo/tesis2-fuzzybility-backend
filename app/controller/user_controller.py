from app.resource import Rpta
from app.data_access import CommandDB, transactional
from typing import List, Dict

from app.data_access.user_data_access import UserDataAccess

from app.dto.user_dto import UserDTO


class UserController:

    @transactional
    def get_one(id_user: int) -> Rpta:
        ans = Rpta()
        user = UserDataAccess.get_one(id_user)

        user_dto: UserDTO  # Declaration

        user_dto = UserDTO.from_model(user)

        ans.setOk("El usuario se obtuvo correctamente")
        rptaAux = user_dto.to_json()

        ans.setBody(rptaAux)

        return ans

    @transactional
    def get_all() -> Rpta:
        ans = Rpta()
        lUsers = UserDataAccess.get_all()
        user_list = []
        for user in lUsers:
            user_dto: UserDTO  # Declaration
            user_dto = UserDTO.from_model(user)
            u = user_dto.to_json()
            user_list.append(u)

        res = {
            "user_list": user_list
        }

        ans.setOk("The users were gathered successfully")
        ans.setBody(res)
        return ans

    @transactional
    def login(username: str, password: str):
        rpta = Rpta()
        user = UserDataAccess.login(username, password)
        if user == None:
            rpta.setError(
                "The username or password are incorrect. Please try again")
        else:
            user_dto: UserDTO  # Declaration
            user_dto = UserDTO.from_model(user)
            rpta.setOk('User logged in successfully')
            rptaAux = user_dto.to_json()
            rpta.setBody(rptaAux)
        return rpta

    @transactional
    def create_user(username: str, password: str, first_name: str, last_name: str, email: str,
                    birth_date, sex: str, highest_education: str, occupation: str, active_status: bool):
        rpta = Rpta()
        ans = UserDataAccess.create_one(username, password, first_name, last_name, email,
                                        birth_date, sex, highest_education, occupation, active_status)
        if ans:
            rpta.setOk('User was created successfully')
        else:
            rpta.setError('Username or email are being used')

        return rpta

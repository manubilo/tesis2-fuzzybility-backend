from app.resource import Rpta
from app.data_access import CommandDB, transactional
from typing import List, Dict

from app.data_access.project_data_access import ProjectDataAccess
from app.data_access.questionnaire_data_access import QuestionnaireDataAccess
from app.dto.project_dto import ProjectDTO

from app.data_access.role_x_user_x_project_data_access import Role_X_User_X_ProjectDataAccess
from app.data_access.user_x_questionnaire_x_project_data_access import User_X_Questionnaire_X_ProjectDataAccess
from app.data_access.subattribute_x_questionnaire_x_user_x_project_data_access import Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess
from app.data_access.question_x_subattribute_x_questionnaire_x_user_x_project_data_access import Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess
from app.data_access.user_data_access import UserDataAccess


class ProjectController:

    @transactional
    def create(id_user: int, title: str, website_name: str,
               website_url: str, website_description: str,
               website_justification: str,
               participants_list, questionnaires_list) -> Rpta:
        rpta = Rpta()

        id_user_creator = id_user

        # Primer paso: Crear proyecto y obtener el id_project
        id_project = ProjectDataAccess.create(title, website_name,
                                              website_url, website_description, website_justification)

        # Segundo paso: Insertar nueva fila en t_role_x_user_x_project
        Role_X_User_X_ProjectDataAccess.create(id_user_creator, 1, id_project)

        # Tercer paso: Ir a t_user_x_questionnaire_x_project y buscar
        # algun cruce entre id_user y id_questionnaire.

        # Si lo encuentro y tiene id_project = null, hacer update y colocar
        # el id_project creado
        # Caso contrario, hacer un insert por cada id_questionnaire que no
        # tenga id_project = null

        for questionnaire in questionnaires_list:
            id_questionnaire = questionnaire["id_questionnaire"]
            # Obtener el id_project de la ultima coincidencia de id_user y id_questionnaire
            last_id_project = User_X_Questionnaire_X_ProjectDataAccess.get_last_record_one(
                id_user_creator, id_questionnaire)[-1].id_project
            id_user_x_questionnaire_x_project = User_X_Questionnaire_X_ProjectDataAccess.get_last_record_one(
                id_user_creator, id_questionnaire)[-1].id_user_x_questionnaire_x_project
            if last_id_project:
                User_X_Questionnaire_X_ProjectDataAccess.create(
                    id_user_creator, id_project, id_questionnaire)
            else:
                User_X_Questionnaire_X_ProjectDataAccess.update(
                    id_project, id_user_x_questionnaire_x_project)

        # Cuarto paso: Por cada uno de los participantes en participants_list
        # Obtener el id_user en base al email
        for user in participants_list:
            email = user["email"]
            executor_role = user["executor_role"]
            revisor_role = user["revisor_role"]
            id_user = UserDataAccess.get_one_by_email(email).id_user
            # Insertar en t_role_x_user_x_project el id_user, id_role y
            # el id_project
            if executor_role == 1:
                Role_X_User_X_ProjectDataAccess.create(id_user, 2, id_project)
            elif revisor_role == 1:
                Role_X_User_X_ProjectDataAccess.create(id_user, 3, id_project)
            # Ahora en t_user_x_questionnaire_x_project
            # Por cada cuestionario seleccionado, obtener el id_questionnaire
            # Insertar el id_user, id_project y el id_questionnaire
            for questionnaire in questionnaires_list:
                id_questionnaire = questionnaire["id_questionnaire"]
                id_user_x_questionnaire_x_project = User_X_Questionnaire_X_ProjectDataAccess.create(
                    id_user, id_project, id_questionnaire)

                # Como tengo que ingresar el id_subattribute en t_subattribute_x_quetionnaire_x_user_x_project
                # Tengo que obtener la lista de subattributos del usuario creador del cuestionario

                # Obtengo el id_user_x_questionnaire_x_project del usuario creador del cuestionario para buscar las preguntas asociadas a ese usuario
                id_user_x_questionnaire_x_project_creator = User_X_Questionnaire_X_ProjectDataAccess.get_one(
                    id_user_creator, id_questionnaire).id_user_x_questionnaire_x_project

                lSubattributes = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_subattributes_questionnaire(
                    id_user_x_questionnaire_x_project_creator)
                for subattribute in lSubattributes:
                    id_subattribute = subattribute.id_subattribute

                    id_subattribute_x_questionnaire_x_user_x_project = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.create(
                        id_user_x_questionnaire_x_project, id_subattribute)

                    # Obtengo el id_subattribute_x_questionnaire_x_user_project del usuario creador del cuestionario
                    id_subattribute_x_questionnaire_x_user_x_project_creator = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_one(
                        id_user_x_questionnaire_x_project_creator, id_subattribute).id_subattribute_x_questionnaire_x_user_x_project

                    # Necesito los id_question. Utilizo el id_subattribute_x_questionnaire_x_user_x_project del usuario creador del cuestionario
                    lQuestions = Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_questions_questionnaire(
                        id_subattribute_x_questionnaire_x_user_x_project_creator)
                    for question in lQuestions:
                        id_question = question.id_question
                        Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.create(
                            id_subattribute_x_questionnaire_x_user_x_project, id_question)

        rpta.setOk("Project was created successfully")
        return rpta

    @transactional
    def list(id_user: int) -> Rpta:
        ans = Rpta()

        lProjects = ProjectDataAccess.get_all_by_id_user(id_user)

        # Obtener el rol del proyecto

        project_list = []
        for project in lProjects:
            project_dto: ProjectDTO  # Declaration
            project_dto = ProjectDTO.from_model(project)
            p = project_dto.to_json()
            role_x_users = Role_X_User_X_ProjectDataAccess.get_all(
                project.id_project, id_user)
            for u in role_x_users:
                id_role = u.id_role
                pro = {
                    "role": id_role,
                    "project": p,
                }
                project_list.append(pro)

        res = {
            "id_user": id_user,
            "project_list": project_list
        }

        ans.setOk("The user's projects were gathered successfully")
        # ansAux = res.to_json()
        ans.setBody(res)

        return ans

    @transactional
    def list_projects_with_questionnaires(id_user: int, id_project: int) -> Rpta:
        ans = Rpta()

        questionnaire_report_list = []
        # Obtener los cuestionarios asociados al proyecto
        l_u_x_q_p = User_X_Questionnaire_X_ProjectDataAccess.get_all_questionnaires_by_project_and_user(
            id_project, id_user
        )
        print("l_u_x_q_p", l_u_x_q_p)
        for questionnaire in l_u_x_q_p:
            score = questionnaire.score
            if score != None:
                id_questionnaire = questionnaire.id_questionnaire
                quest = QuestionnaireDataAccess.get_one(id_questionnaire)
                name = quest.name
                q = {
                    "id_questionnaire": id_questionnaire,
                    "name": name,
                    "score": score
                }
                questionnaire_report_list.append(q)

        res = {
            "id_user": id_user,
            "id_project": id_project,
            "questionnaires_report_list": questionnaire_report_list
        }

        ans.setOk("The user's project for reports were gathered successfully")
        ans.setBody(res)

        return ans

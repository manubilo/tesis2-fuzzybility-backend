from app.resource import Rpta
from app.data_access import CommandDB, transactional
from typing import List, Dict
from datetime import datetime

from app.data_access.questionnaire_data_access import QuestionnaireDataAccess
from app.data_access.subattribute_data_access import SubattributeDataAccess
from app.data_access.question_data_access import QuestionDataAccess
from app.data_access.user_x_questionnaire_x_project_data_access import User_X_Questionnaire_X_ProjectDataAccess
from app.data_access.subattribute_x_questionnaire_x_user_x_project_data_access import Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess
from app.data_access.question_x_subattribute_x_questionnaire_x_user_x_project_data_access import Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess
from app.data_access.subattribute_x_questionnaire_x_user_x_project_data_access import Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess
from app.data_access.question_x_subattribute_x_questionnaire_x_user_x_project_data_access import Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess
from app.data_access.role_x_user_x_project_data_access import Role_X_User_X_ProjectDataAccess
from app.data_access.project_data_access import ProjectDataAccess

from app.dto.questionnaire_dto import QuestionnaireDTO
from app.dto.subattribute_dto import SubattributeDTO
from app.dto.question_dto import QuestionDTO

from app.fuzzy_logic.fuzzy_logic_module import FuzzyLogicModule


class QuestionnaireController:

    @transactional
    def create(id_user: int, name: str, likert_number: int, subattribute_list) -> Rpta:
        rpta = Rpta()

        # Primer paso: Crear cuestionario en t_questionnaire
        id_questionnaire = QuestionnaireDataAccess.create(name, likert_number)

        # Segundo paso: Insertar en t_user_x_questionnaire_x_project
        id_user_x_questionnaire_x_project = User_X_Questionnaire_X_ProjectDataAccess.create(
            id_user, None, id_questionnaire)

        for subattribute in subattribute_list:
            name = subattribute["name"]
            # Tercer paso: Crear cada atributo en t_subattribute
            id_subattribute = SubattributeDataAccess.create(name)
            # Cuarto paso: Insertar en t_subattribute_x_questionnaire_x_user_x_project
            id_subattribute_x_questionnaire_x_user_x_project = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.create(
                id_user_x_questionnaire_x_project, id_subattribute)

            questions_list = subattribute["questions_list"]
            # Quinto paso: Crear cada pregunta y asignarlo al id_subattribute creado
            for question in questions_list:
                description = question["description"]
                statement_type = question["statement_type"]
                id_question = QuestionDataAccess.create(
                    description, statement_type)
                # Sexto paso: Insertar en t_q_x_subatt_x_questionnaire_x_user_x_project
                Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.create(
                    id_subattribute_x_questionnaire_x_user_x_project, id_question)
        print("termine bien de crear el cuestionario")
        rpta.setOk("Questionnaire was created successfully")
        return rpta

    @transactional
    def list_questionnaires(id_project: int, id_user: int) -> Rpta:
        rpta = Rpta()
        questionnaires_list = []

        # Primer paso: Obtener la lista de cuestionarios asociados al usuario y al proyecto
        lq = QuestionnaireDataAccess.list_questionnaires(id_project, id_user)

        for questionnaire in lq:
            id_questionnaire = questionnaire.id_questionnaire

            # Obtengo los comentarios asociados al proyecto
            score_comment = (User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
                id_project, id_user, id_questionnaire)).score_comment
            improvement_comment = (User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
                id_project, id_user, id_questionnaire)).improvement_comment
            general_comment = (User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
                id_project, id_user, id_questionnaire)).general_comment
            evaluation_date = (User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
                id_project, id_user, id_questionnaire)).evaluation_date
            # evaluation_date = "2020-10-12 00:00:00"
            score = (User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
                id_project, id_user, id_questionnaire)).score
            status = (User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
                id_project, id_user, id_questionnaire)).status

            questionnaire_dto: QuestionnaireDTO  # Declaration
            questionnaire_dto = QuestionnaireDTO.from_model(questionnaire)
            q = questionnaire_dto.to_json()
            quest = {
                "id_questionnaire": id_questionnaire,
                "name": q["name"],
                "creation_date": q["creation_date"],
                "evaluation_date": evaluation_date.__str__(),
                "likert_number": q["likert_number"],
                "score": score,
                "status": status,
                "score_comment": score_comment,
                "improvement_comment": improvement_comment,
                "general_comment": general_comment,
            }
            questionnaires_list.append(quest)
        res = {
            "id_user": id_user,
            "id_project": id_project,
            "questionnaires_list": questionnaires_list
        }

        rpta.setOk("The project's questionnaires were gathered successfully")
        rpta.setBody(res)

        return rpta

    @transactional
    def list_questionnaires_by_user(id_user: int) -> Rpta:
        rpta = Rpta()
        questionnaires_list = []

        # Primer paso: Obtener la lista de cuestionarios asociados al usuario
        lq = QuestionnaireDataAccess.list_questionnaires_by_user(id_user)

        for questionnaire in lq:

            questionnaire_dto: QuestionnaireDTO  # Declaration
            questionnaire_dto = QuestionnaireDTO.from_model(questionnaire)
            q = questionnaire_dto.to_json()
            quest = {
                "id_questionnaire": q["id_questionnaire"],
                "name": q["name"],
                "creation_date": q["creation_date"],
                "likert_number": q["likert_number"]
            }
            questionnaires_list.append(quest)
        res = {
            "id_user": id_user,
            "questionnaires_list": questionnaires_list
        }
        rpta.setOk("The user's questionnaires were gathered successfully")
        rpta.setBody(res)

        return rpta

    @transactional
    def list_questions(id_project: int, id_user: int, id_questionnaire: int) -> Rpta:
        ans = Rpta()
        questions_list = []
        subattributes_list = []

        id_user_x_questionnaire_x_project = User_X_Questionnaire_X_ProjectDataAccess.get_one(
            id_user, id_questionnaire).id_user_x_questionnaire_x_project

        lSubattributes = SubattributeDataAccess.list_subattributes(
            id_user_x_questionnaire_x_project)

        for subattribute in lSubattributes:
            id_subattribute = subattribute.id_subattribute

            id_subattribute_x_questionnaire_x_user_x_project = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_one(
                id_user_x_questionnaire_x_project, id_subattribute).id_subattribute_x_questionnaire_x_user_x_project

            lQuestions = QuestionDataAccess.list_questions(
                id_subattribute_x_questionnaire_x_user_x_project)

            for question in lQuestions:
                question_dto: QuestionDTO  # Declaration
                question_dto = QuestionDTO.from_model(question)
                qn = question_dto.to_json()
                questions_list.append(qn)

            subattribute_dto: SubattributeDTO  # Declaration
            subattribute_dto = SubattributeDTO.from_model(subattribute)
            s = subattribute_dto.to_json()
            res1 = {
                "id_subattribute": id_subattribute,
                "name": s["name"],
                "questions_list": questions_list
            }

            questions_list = []
            subattributes_list.append(res1)

        res2 = {
            "id_user": id_user,
            "id_project": id_project,
            "id_questionnaire": id_questionnaire,
            "subattribute_list": subattributes_list
        }

        ans.setOk("The questionnaire's questions were gathered successfully")
        ans.setBody(res2)

        return ans

    def answer(id_user: int, id_questionnaire: int, id_project: int, subattribute_list):
        rpta = Rpta()

        # Obtener el id_user_x_questionnaire_x_project

        l_id_user_x_questionnaire_x_project = User_X_Questionnaire_X_ProjectDataAccess.get_last_records(
            id_user, id_questionnaire, id_project)

        print("l_id_user_x_questionnaire_x_project",
              l_id_user_x_questionnaire_x_project)

        for reg in l_id_user_x_questionnaire_x_project:
            # Debería haber un máximo de 2 registros (diseñador y ejecutor)
            print("lista l_id_user_x_questionnaire_x_project",
                  l_id_user_x_questionnaire_x_project)

            id_user_x_questionnaire_x_project = reg.id_user_x_questionnaire_x_project

            print("id_user_x_questionnaire_x_project",
                  id_user_x_questionnaire_x_project)

            print("subattribute_list", subattribute_list)

            # Updatear el campo answer en t_subattribute_x_questonnaire_x_user_x_project
            for subattribute in subattribute_list:
                print("subattribute", subattribute)
                id_subattribute = subattribute["id_subattribute"]
                for question in subattribute["questions_list"]:
                    id_question = question["id_question"]
                    print("id_question", id_question)
                    if(question["answer"]):
                        ans = question["answer"]
                    print("ans = ", ans)
                    # Obtener el id_subattribute_x_questionnaire_x_user_x_project
                    try:
                        id_subattribute_x_questionnaire_x_user_x_project = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_one(
                            id_user_x_questionnaire_x_project, id_subattribute).id_subattribute_x_questionnaire_x_user_x_project
                        if (id_subattribute_x_questionnaire_x_user_x_project):
                            print("grabando respuestas")
                            print("id_subattribute_x_questionnaire_x_user_x_project",
                                  id_subattribute_x_questionnaire_x_user_x_project)
                            Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.answer(
                                ans, id_subattribute_x_questionnaire_x_user_x_project, id_question)
                    except:
                        print("no se encontro el id")
                        # Marcar el cuestionario como completo en t_user_x_questionnaire_x_project
            User_X_Questionnaire_X_ProjectDataAccess.update_flag(
                id_user_x_questionnaire_x_project)

            # Updatear la fecha de evaluación
            # print ("fecha evaluacion", datetimenow.strftime("%Y-%m-%d %H:%M:%S"))
            User_X_Questionnaire_X_ProjectDataAccess.update_evaluation_date(
                id_user_x_questionnaire_x_project, datetime.now())

        # Si verifico que todos los usuarios con rol ejecutor han respondido el cuestionario
        # Llamar al módulo de fuzzificación y desfuzzificación
        all_questionnaires_completed = QuestionnaireController.checkQuestionnairesCompleted(
            id_user, id_questionnaire, id_project)

        if all_questionnaires_completed:
            total_score = QuestionnaireController.get_total_score(
                id_questionnaire, id_project)
            # total_score = 100
            print("total score", total_score)
            users_by_questionnaire_by_project = User_X_Questionnaire_X_ProjectDataAccess.get_users_by_questionnaire_by_project(
                id_questionnaire, id_project)
            for u in users_by_questionnaire_by_project:
                id_u = u.id_user
                # Updatear el score con total_score
                User_X_Questionnaire_X_ProjectDataAccess.update_score(
                    id_u, id_project, total_score)
                # Updatear el status por Completo
                User_X_Questionnaire_X_ProjectDataAccess.update_status(
                    id_u, id_project,  "Completo")

        # OJO: VERIFICAR SI TODOS LOS CUESTIONARIOS ASOCIADOS A UN PROYECTO TIENEN EL ESTADO COMPLETO.
        # EN ESE CASO, COLOCAR ESTADO COMPLETO EN EL PROYECTO
        project_questionnaires_completed_list = User_X_Questionnaire_X_ProjectDataAccess.check_project_questionnaires_completed(
            id_project)

        print("project_questionnaires_completed_list",
              project_questionnaires_completed_list)
        if len(project_questionnaires_completed_list) == 0:
            ProjectDataAccess.update_status(id_project)

        rpta.setOk("Questionnaire was answered successfully")
        return rpta

    def checkQuestionnairesCompleted(id_user: int, id_questionnaire: int, id_project: int):
        rpta = True

        # Obtener todos los usuarios asociados al proyecto que no han respondido el cuestionario
        project_users = User_X_Questionnaire_X_ProjectDataAccess.get_project_users_not_completed(
            id_questionnaire, id_project)

        print("usuarios que no han respondido", project_users)
        for u in project_users:
            id_u = u.id_user
            print("id_usuario que no han respondido", id_u)
            # Si alguno de estos usuarios tiene el rol 2 (Ejecutor), entonces falta responder por lo menos 1 cuestionario
            exists_executor_user = Role_X_User_X_ProjectDataAccess.get_executor_user(
                id_project, id_u)
            print("exists_executor_user", exists_executor_user)
            if exists_executor_user:
                rpta = False
        # Caso contrario, devolver True pues se han respondido todos los cuestionarios

        return rpta

    def get_total_score(id_questionnaire: int, id_project: int):
        # Este módulo me devuelve el valor cuantitativo de la usabilidad
        # Luego de llamar al módulo de fuzzificación y desfuzzificación
        # Debe ser llamado una vez que todos los usuarios han respondido
        # todas las preguntas del cuestionario de medición

        # Esta lista llamada answers tendrá las respuestas de los usuarios ejecutores del cuestionarios asociado al proyecto
        answers = []

        # Obtener las respuestas de todos los usuarios en sus propios cuestionarios
        users = User_X_Questionnaire_X_ProjectDataAccess.get_all(
            id_questionnaire, id_project)

        print("users", users)

        # Quitar el primero de users, porque corresponde con el rol de diseñador
        dummy = users.pop(0)

        print("users", users)
        lExecutor_users = []
        for u in users:
            id_u = u.id_user
            # Si alguno de estos usuarios tiene el rol 2 (Ejecutor), entonces falta responder por lo menos 1 cuestionario
            l_r_user = Role_X_User_X_ProjectDataAccess.get_all(
                id_project, id_u)
            print("l_r_user", l_r_user)
            for r in l_r_user:
                role = r.id_role
                if role == 2:
                    lExecutor_users.append(u)

        print("lExecutor_users", lExecutor_users)

        for u in lExecutor_users:
            questions_list = []
            id_user_x_questionnaire_x_project = u.id_user_x_questionnaire_x_project

            lSubattribute_x_questionnaire_x_user_x_project = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_all(
                id_user_x_questionnaire_x_project)

            print("lSubattribute_x_questionnaire_x_user_x_project",
                  lSubattribute_x_questionnaire_x_user_x_project)
            for s in lSubattribute_x_questionnaire_x_user_x_project:
                answers_list = []
                id_subattribute_x_questionnaire_x_user_x_project = s.id_subattribute_x_questionnaire_x_user_x_project
                print("id_subattribute_x_questionnaire_x_user_x_project",
                      id_subattribute_x_questionnaire_x_user_x_project)
                lQuestions = Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_questions_questionnaire(
                    id_subattribute_x_questionnaire_x_user_x_project)
                for q in lQuestions:
                    answers_list.append(q.answer)
                questions_list.append(answers_list)
            answers.append(questions_list)
        print("answers", answers)
        # Enviar al modulo de fuzzificación y desfuzzificación
        total_score = FuzzyLogicModule.get_score(answers)
        # total_score = 98

        # Updatear el valor total_score en la tabla questionnaire
        # QuestionnaireDataAccess.update_total_score(id_questionnaire, total_score)

        return total_score

    def comment(id_user: int, id_questionnaire: int, id_project: int, score_comment: str, improvement_comment: str, general_comment: str):
        rpta = Rpta()
        print("entre a comment")
        id_user_x_questionnaire_x_project = User_X_Questionnaire_X_ProjectDataAccess.get_one_with_project(
            id_project, id_user, id_questionnaire).id_user_x_questionnaire_x_project

        User_X_Questionnaire_X_ProjectDataAccess.update_score_comment(
            id_user_x_questionnaire_x_project, score_comment)
        User_X_Questionnaire_X_ProjectDataAccess.update_improvement_comment(
            id_user_x_questionnaire_x_project, improvement_comment)
        User_X_Questionnaire_X_ProjectDataAccess.update_general_comment(
            id_user_x_questionnaire_x_project, general_comment)
        print("commented")

        rpta.setOk("Questionnaire was commented successfully")
        return rpta

    def get_answers(id_questionnaire: int, id_project: int):
        rpta = Rpta()
        print("getting answers")

        # Este módulo es llamado cuando se quiere visualizar las respuestas efectuadas por los usuarios del proyecto
        # Así como también, los cálculos del algoritmo de lógica difusa

        # Esta lista llamada answers tendrá las respuestas de los usuarios ejecutores del cuestionarios asociado al proyecto
        answers = []

        # Obtener las respuestas de todos los usuarios en sus propios cuestionarios
        users = User_X_Questionnaire_X_ProjectDataAccess.get_all(
            id_questionnaire, id_project)

        print("users", users)

        # Quitar el primero de users, porque corresponde con el rol de diseñador
        dummy = users.pop(0)

        print("users", users)
        lExecutor_users = []
        for u in users:
            id_u = u.id_user
            # Si alguno de estos usuarios tiene el rol 2 (Ejecutor), entonces falta responder por lo menos 1 cuestionario
            l_r_user = Role_X_User_X_ProjectDataAccess.get_all(
                id_project, id_u)
            print("l_r_user", l_r_user)
            for r in l_r_user:
                role = r.id_role
                if role == 2:
                    lExecutor_users.append(u)

        print("lExecutor_users", lExecutor_users)

        subattributes_list = []
        for u in lExecutor_users:
            questions_list = []
            id_user_x_questionnaire_x_project = u.id_user_x_questionnaire_x_project

            # List subattributes
            if(subattributes_list == []):
                subattributes_list = SubattributeDataAccess.list_subattributes(
                    id_user_x_questionnaire_x_project)

            print("subattributes_list", subattributes_list)

            lSubattribute_x_questionnaire_x_user_x_project = Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_all(
                id_user_x_questionnaire_x_project)

            print("lSubattribute_x_questionnaire_x_user_x_project",
                  lSubattribute_x_questionnaire_x_user_x_project)

            q_list = []
            for s in lSubattribute_x_questionnaire_x_user_x_project:
                answers_list = []
                id_subattribute_x_questionnaire_x_user_x_project = s.id_subattribute_x_questionnaire_x_user_x_project
                # Obtener el nombre del subatributo

                print("id_subattribute_x_questionnaire_x_user_x_project",
                      id_subattribute_x_questionnaire_x_user_x_project)

                # List questions
                q_list_subattribute = QuestionDataAccess.list_questions(
                    id_subattribute_x_questionnaire_x_user_x_project)
                q_list.append(q_list_subattribute)

                print("q_list", q_list)

                lQuestions = Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess.get_questions_questionnaire(
                    id_subattribute_x_questionnaire_x_user_x_project)
                for q in lQuestions:
                    # Obtener el nombre de la pregunta
                    answers_list.append(q.answer)
                questions_list.append(answers_list)
            answers.append(questions_list)
        print("answers", answers)

        subfactor_list = FuzzyLogicModule.get_subfactor_score(answers)
        factor_list = FuzzyLogicModule.get_factor_score(subfactor_list)

        print("questions_list", questions_list)

        subattributes_names = []
        for s in subattributes_list:
            subattributes_names.append(s.name)

        print("q_list", q_list)

        question_names = []
        for q in q_list:
            auxList = []
            for q2 in q:
                print("q2", q2)
                auxList.append(q2.description)
            question_names.append(auxList)
        print("question_names", question_names)

        factors = []
        for i in range(len(subattributes_names)):

            factor = {
                "factor_name": subattributes_names[i],
                "factor_score": factor_list[i],
                "subfactor_name": question_names[i],
                "subfactor_score": subfactor_list[i]
            }
            factors.append(factor)

        print("factors", factors)

        answers = {
            "id_questionnaire": id_questionnaire,
            "id_project": id_project,
            "answers": answers,
            "factors": factors
        }

        res = {
            "answers": answers
        }

        rpta.setOk("The answers were gathered successfully")
        rpta.setBody(res)

        return rpta

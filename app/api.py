
from flask_restful import Api
from flask import Flask
from flask_cors import CORS, cross_origin
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

from app.resource.questionnaire_resource import *
from app.resource.project_resource import *
from app.resource.user_resource import *
from app.models.question_x_subattribute_x_questionnaire_x_user_x_project import Question_X_Subattribute_X_Questionnaire_X_User_X_Project
from app.models.subattribute_x_questionnaire_x_user_x_project import Subattribute_X_Questionnaire_X_User_X_Project
from app.models.user_x_questionnaire_x_project import User_X_Questionnaire_X_Project
from app.models.role_x_user_x_project import Role_X_User_X_Project
from app.models.question import Question
from app.models.subattribute import Subattribute
from app.models.questionnaire import Questionnaire
from app.models.project import Project
from app.models.user import User
from app.models.role import Role
from app.models import db


migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

# MODELS

# USER SERVICES
api.add_resource(UserResourceGetOne, '/api/user/getone')
api.add_resource(UserResourceGetAll, '/api/user/getall')
api.add_resource(UserResourceCreate, '/api/user/create')
api.add_resource(UserResourceLogin, '/api/user/login')

# PROJECT SERVICES
api.add_resource(ProjectResourceCreate, '/api/project/create')
api.add_resource(ProjectResourceList, '/api/project/list')

# QUESTIONNAIRES SERVICES
api.add_resource(QuestionnaireResourceCreate, '/api/questionnaire/create')
api.add_resource(QuestionnaireResourceListQuestionnaires,
                 '/api/questionnaire/listQuestionnaires')
api.add_resource(QuestionnaireResourceListQuestionnairesByUser,
                 '/api/questionnaire/listQuestionnairesByUser')
api.add_resource(QuestionnaireResourceListQuestions,
                 '/api/questionnaire/listQuestions')
api.add_resource(QuestionnaireResourceAnswer, '/api/questionnaire/answer')
api.add_resource(QuestionnaireResourceComment, '/api/questionnaire/comment')
api.add_resource(QuestionnaireResourceGetAnswers,
                 '/api/questionnaire/getAnswers')


# REPORTS SERVICES
api.add_resource(ProjectResourceListProjectsWithQuestionnaires,
                 '/api/reports/listProjectsWithQuestionnaires')

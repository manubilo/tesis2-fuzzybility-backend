from . import db
from sqlalchemy import func
from app.models.role import Role
from app.models.user import User
from app.models.project import Project

class Role_X_User_X_Project(db.Model):
    __tablename__ = "T_ROLE_X_USER_X_PROJECT"
    id_role_x_user_x_project = db.Column('id_role_x_user_x_project', db.Integer, primary_key = True)
    id_role = db.Column('id_role', db.ForeignKey(Role.id_role)) #FK
    id_user = db.Column('id_user', db.ForeignKey(User.id_user)) #FK
    id_project = db.Column('id_project', db.ForeignKey(Project.id_project)) #FK
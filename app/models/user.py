from . import db
from sqlalchemy import func
from app.models.role import Role

class User(db.Model):
    __tablename__ = "T_USER"
    id_user = db.Column('id_user', db.Integer, primary_key = True)
    username = db.Column('username', db.String(100), nullable = True)
    password = db.Column('password', db.String(100), nullable = True)
    first_name = db.Column('first_name', db.String(120), nullable = True)
    last_name = db.Column('last_name', db.String(170), nullable = True)
    email = db.Column('email', db.String(100), nullable = True)
    birth_date = db.Column('birth_date', db.DateTime, nullable = True)
    sex = db.Column('sex', db.String(100), nullable = True)
    highest_education = db.Column('highest_education', db.String(100), nullable = True)
    occupation = db.Column('occupation', db.String(100), nullable = True)
    creation_date = db.Column('creation_date', db.DateTime, nullable = True, default = func.current_timestamp())
    active_status = db.Column('active_status', db.Boolean, nullable = True, default = True)
from . import db
from sqlalchemy import func
from app.models.questionnaire import Questionnaire

class Subattribute(db.Model):
    __tablename__ = "T_SUBATTRIBUTE"
    id_subattribute = db.Column('id_subattribute', db.Integer, primary_key = True)
    name = db.Column('name', db.String(150), nullable = True)
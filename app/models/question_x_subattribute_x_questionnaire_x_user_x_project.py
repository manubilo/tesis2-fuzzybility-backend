from . import db
from sqlalchemy import func
from app.models.question import Question
from app.models.subattribute_x_questionnaire_x_user_x_project import Subattribute_X_Questionnaire_X_User_X_Project


class Question_X_Subattribute_X_Questionnaire_X_User_X_Project(db.Model):
    __tablename__ = "T_Q_X_SUBATT_X_QUESTIONNAIRE_X_USER_X_PROJECT"
    id_question_x_subattribute_x_questionnaire_x_user_x_project = db.Column('id_question_x_subattribute_x_questionnaire_x_user_x_project', db.Integer, primary_key = True)
    id_subattribute_x_questionnaire_x_user_x_project = db.Column('id_subattribute_x_questionnaire_x_user_x_project', db.ForeignKey(Subattribute_X_Questionnaire_X_User_X_Project.id_subattribute_x_questionnaire_x_user_x_project)) #FK
    id_question = db.Column('id_question', db.ForeignKey(Question.id_question)) #FK
    answer = db.Column('answer', db.Integer, nullable = True)
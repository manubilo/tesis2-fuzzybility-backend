from . import db
from app.models.project import Project
from sqlalchemy import func

class Questionnaire(db.Model):
    __tablename__ = "T_QUESTIONNAIRE"
    id_questionnaire = db.Column('id_questionnaire', db.Integer, primary_key = True)
    name = db.Column('name', db.String(150), nullable = True)
    creation_date = db.Column('creation_date', db.DateTime, nullable = True, default = func.current_timestamp())
    likert_number = db.Column('likert_number', db.Integer, nullable = True)
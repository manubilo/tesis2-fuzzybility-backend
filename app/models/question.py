from . import db
from app.models.subattribute import Subattribute

class Question(db.Model):
    __tablename__ = "T_QUESTION"
    id_question = db.Column('id_question', db.Integer, primary_key = True)
    description = db.Column('description', db.String(200), nullable = True)
    statement_type = db.Column('statement_type', db.Boolean, nullable = True)
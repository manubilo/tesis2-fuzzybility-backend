from . import db

class Role(db.Model):
    __tablename__ = "T_ROLE"
    id_role = db.Column('id_role', db.Integer, primary_key = True)
    name = db.Column('name', db.String(50))
    description = db.Column('description', db.String(200))

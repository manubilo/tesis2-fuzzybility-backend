from . import db
from sqlalchemy import func
from app.models.questionnaire import Questionnaire
from app.models.user import User
from app.models.project import Project

class User_X_Questionnaire_X_Project(db.Model):
    __tablename__ = "T_USER_X_QUESTIONNAIRE_X_PROJECT"
    id_user_x_questionnaire_x_project = db.Column('id_user_x_questionnaire_x_project', db.Integer, primary_key = True)
    id_user = db.Column('id_user', db.ForeignKey(User.id_user)) #FK
    id_questionnaire = db.Column('id_questionnaire', db.ForeignKey(Questionnaire.id_questionnaire)) #FK
    id_project = db.Column('id_project', db.ForeignKey(Project.id_project)) #FK
    score_comment = db.Column('score_comment', db.String(500), nullable = True)
    improvement_comment = db.Column('improvement_comment', db.String(500), nullable = True)
    general_comment = db.Column('general_comment', db.String(500), nullable = True)
    flg_questionnaire_completed = db.Column('flg_questionnaire_completed', db.Integer, nullable = True)
    evaluation_date = db.Column('evaluation_date', db.DateTime, nullable = True)
    score = db.Column('score', db.Integer, nullable = True)
    status = db.Column('status', db.String(50), nullable = True)
    
from . import db
from sqlalchemy import func
from app.models.user import User

class Project(db.Model):
    __tablename__ = "T_PROJECT"
    id_project = db.Column('id_project', db.Integer, primary_key = True)
    title = db.Column('title', db.String(100), nullable = True)
    creation_date = db.Column('creation_date', db.DateTime, nullable = True, default = func.current_timestamp())
    status = db.Column('status', db.String(50), nullable = True)
    website_name = db.Column('website_name', db.String(100), nullable = True)
    website_url = db.Column('website_url', db.String(100), nullable = True)
    website_description = db.Column('website_description', db.String(500), nullable = True)
    website_justification = db.Column('website_justification', db.String(500), nullable = True)
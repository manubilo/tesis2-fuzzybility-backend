from . import db
from sqlalchemy import func
from app.models.subattribute import Subattribute
from app.models.user_x_questionnaire_x_project import User_X_Questionnaire_X_Project


class Subattribute_X_Questionnaire_X_User_X_Project(db.Model):
    __tablename__ = "T_SUBATTRIBUTE_X_QUESTIONNAIRE_X_USER_X_PROJECT"
    id_subattribute_x_questionnaire_x_user_x_project = db.Column('id_subattribute_x_questionnaire_x_user_x_project', db.Integer, primary_key = True)
    id_user_x_questionnaire_x_project = db.Column('id_user_x_questionnaire_x_project', db.ForeignKey(User_X_Questionnaire_X_Project.id_user_x_questionnaire_x_project)) #FK
    id_subattribute = db.Column('id_subattribute', db.ForeignKey(Subattribute.id_subattribute)) #FK

    
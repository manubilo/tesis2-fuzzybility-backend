from app.models import db
from app.models.subattribute_x_questionnaire_x_user_x_project import Subattribute_X_Questionnaire_X_User_X_Project
from app.models.subattribute import Subattribute

class Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess:
    def create(id_user_x_questionnaire_x_project : int, id_subattribute: int):
        try:
            subattribute_X_Questionnaire_X_User_X_Project : Subattribute_X_Questionnaire_X_User_X_Project
            subattribute_X_Questionnaire_X_User_X_Project = Subattribute_X_Questionnaire_X_User_X_Project(
                id_user_x_questionnaire_x_project = id_user_x_questionnaire_x_project,
                id_subattribute = id_subattribute
            )
            db.session.add(subattribute_X_Questionnaire_X_User_X_Project)
            db.session.flush()

            return subattribute_X_Questionnaire_X_User_X_Project.id_subattribute_x_questionnaire_x_user_x_project
        except Exception as e:
            raise e

    def get_one(id_user_x_questionnaire_x_project : int, id_subattribute : int):
        try:
            return Subattribute_X_Questionnaire_X_User_X_Project.query.filter_by(id_user_x_questionnaire_x_project = id_user_x_questionnaire_x_project, id_subattribute = id_subattribute).first() 
        except Exception as e:
            raise e

    def get_all(id_user_x_questionnaire_x_project : int):
        try:
            return Subattribute_X_Questionnaire_X_User_X_Project.query.filter_by(id_user_x_questionnaire_x_project = id_user_x_questionnaire_x_project).all() 
        except Exception as e:
            raise e


    def get_subattributes_questionnaire(id_user_x_questionnaire_x_project : int):
        try:
            return Subattribute_X_Questionnaire_X_User_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project = id_user_x_questionnaire_x_project
            ).all()
        except Exception as e:
            raise e


    



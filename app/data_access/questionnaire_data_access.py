from app.models import db
from typing import Dict, List, Tuple

from app.models.user_x_questionnaire_x_project import User_X_Questionnaire_X_Project

from app.models.questionnaire import Questionnaire

class QuestionnaireDataAccess:

    def create(name : str, likert_number : int) -> int:
        try:
            questionnaire : Questionnaire
            questionnaire = Questionnaire(
                name = name,
                likert_number = likert_number
            )
            db.session.add(questionnaire)
            db.session.flush()
            return questionnaire.id_questionnaire
        except Exception as e:
            raise e

    def get_one(id_questionnaire) -> Questionnaire:
        try:
            return Questionnaire.query.filter_by(id_questionnaire = id_questionnaire).first()
        except Exception as e:
            raise e

    def list_questionnaires(id_project : int, id_user : int) -> List[Questionnaire]:
        try:
            return Questionnaire.query.filter(User_X_Questionnaire_X_Project.id_project == id_project, User_X_Questionnaire_X_Project.id_user == id_user, Questionnaire.id_questionnaire == User_X_Questionnaire_X_Project.id_questionnaire).all()
        except Exception as e:
            raise e

    def list_questionnaires_by_user(id_user : int) -> List[Questionnaire]:
        try:
            return Questionnaire.query.filter(User_X_Questionnaire_X_Project.id_user == id_user, Questionnaire.id_questionnaire == User_X_Questionnaire_X_Project.id_questionnaire).all()
        except Exception as e:
            raise e 

    def update_total_score(id_questionnaire : int, total_score : int):
        try:
            questionnaire : Questionnaire
            questionnaire = Questionnaire.query.filter_by(id_questionnaire = id_questionnaire).first()
            questionnaire.total_score = total_score
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

from app.models import db
from typing import Dict, List, Tuple
from app.models.subattribute import Subattribute
from app.models.subattribute_x_questionnaire_x_user_x_project import Subattribute_X_Questionnaire_X_User_X_Project

class SubattributeDataAccess:

    def create(name : str) -> int:
        try:
            subattribute : Subattribute
            subattribute = Subattribute(
                name = name
            )
            db.session.add(subattribute)
            db.session.flush()
            return subattribute.id_subattribute
        except Exception as e:
            raise e

    def list_subattributes(id_user_x_questionnaire_x_project : int) -> List[Subattribute]:
        try:
            return Subattribute.query.filter(Subattribute_X_Questionnaire_X_User_X_Project.id_user_x_questionnaire_x_project == id_user_x_questionnaire_x_project,
                                            Subattribute.id_subattribute == Subattribute_X_Questionnaire_X_User_X_Project.id_subattribute).all()
        except Exception as e:
            raise e
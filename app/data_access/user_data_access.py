from app.models import db
from app.models.user import User
from typing import List, Dict


class UserDataAccess:

    def get_one(id_user: int) -> User:
        try:
            return User.query.filter_by(id_user=id_user).first()
        except Exception as e:
            raise e

    def get_all():
        try:
            return User.query.all()
        except Exception as e:
            raise e

    def get_one_by_email(email: str) -> int:
        try:
            return User.query.filter_by(email=email).first()
        except Exception as e:
            raise e

    def login(username: str, password: str) -> User:
        try:
            return User.query.filter_by(username=username, password=password).first()
        except Exception as e:
            raise e

    def create_one(username: str, password: str, first_name: str, last_name: str, email: str,
                   birth_date, sex: str, highest_education: str, occupation: str, active_status: bool):
        try:
            u = User.query.filter_by(username=username).first()
            u2 = User.query.filter_by(email=email).first()
            if u == None and u2 == None:
                user: User
                user = User(
                    username=username,
                    password=password,
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    birth_date=birth_date,
                    sex=sex,
                    highest_education=highest_education,
                    occupation=occupation,
                    active_status=active_status
                )
                db.session.add(user)
                db.session.flush()
                return True
            else:
                return False
        except Exception as e:
            raise e

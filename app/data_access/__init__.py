from functools import wraps
from app.resource import Rpta
from app.models import db
import traceback

class CommandDB():
    def commit():
        print('commit')
        db.session.commit()
    def rollback():
        print('rollback')
        db.session.rollback()
        
import traceback

def transactional(f):
    """
    This wrapper function makes sure a transaction is commited or rollbacked
    """
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            answer = f(*args, **kwargs)
            db.session.commit()
            print('commit')
            return answer # No errors
        except Exception as e: # Something wrong happened
            db.session.rollback()
            answer = Rpta()
            #print('rollback')
            #print( args[-1:][0])
            #answer.setError(args[-1:][0],'Error : {}'.format(str(e)))
            import os
            answer.setError('Error en la transacción','Error : {}'.format(str(e)))
            raise e
        finally:
            traceback.print_exc()
            return answer
    return wrapper
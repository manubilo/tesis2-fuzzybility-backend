from app.models import db
from app.models.question_x_subattribute_x_questionnaire_x_user_x_project import Question_X_Subattribute_X_Questionnaire_X_User_X_Project

class Question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess:
    
    def create(id_subattribute_x_questionnaire_x_user_x_project : int, id_question : int):
        try:
            question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess : Question_X_Subattribute_X_Questionnaire_X_User_X_Project
            question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess = Question_X_Subattribute_X_Questionnaire_X_User_X_Project(
                id_subattribute_x_questionnaire_x_user_x_project = id_subattribute_x_questionnaire_x_user_x_project,
                id_question = id_question
            )
            db.session.add(question_X_Subattribute_X_Questionnaire_X_User_X_ProjectDataAccess)
            db.session.flush()
        except Exception as e:
            raise e


    def get_one(id_subattribute_x_questionnaire_x_user_x_project : int):
        try:
            return Question_X_Subattribute_X_Questionnaire_X_User_X_Project.query.filter_by(
                id_subattribute_x_questionnaire_x_user_x_project = id_subattribute_x_questionnaire_x_user_x_project).first()
        except Exception as e:
            raise e


    def answer(ans : int, id_subattribute_x_questionnaire_x_user_x_project : int, id_question : int):
        try:
            question_X_Subattribute_X_Questionnaire_X_User_X_Project : Question_X_Subattribute_X_Questionnaire_X_User_X_Project
            question_X_Subattribute_X_Questionnaire_X_User_X_Project = Question_X_Subattribute_X_Questionnaire_X_User_X_Project.query.filter_by(
                id_subattribute_x_questionnaire_x_user_x_project = id_subattribute_x_questionnaire_x_user_x_project, id_question = id_question).first()
            question_X_Subattribute_X_Questionnaire_X_User_X_Project.answer = ans
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def get_questions_questionnaire(id_subattribute_x_questionnaire_x_user_x_project : int):
        try:
            return Question_X_Subattribute_X_Questionnaire_X_User_X_Project.query.filter_by(id_subattribute_x_questionnaire_x_user_x_project = id_subattribute_x_questionnaire_x_user_x_project).all()
        except Exception as e:
            raise e
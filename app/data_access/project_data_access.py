from app.models import db
from typing import Dict, List, Tuple
from app.models.project import Project
from app.models.role_x_user_x_project import Role_X_User_X_Project

class ProjectDataAccess:
    
    def get_one(id_project : int) -> Project:
        try:
            return Project.query.filter_by(id_project = id_project).first()
        except Exception as e:
            raise e

    def get_all_by_id_user(id_user : int) -> List[Project]:
        try:
            return Project.query.filter(Project.id_project == Role_X_User_X_Project.id_project, Role_X_User_X_Project.id_user == id_user).all()
        except Exception as e:
            raise e

    def create(title : str, website_name : str, 
                website_url : str, website_description : str,
                 website_justification : str) -> int:
        try:
            project : Project
            project = Project(
                title = title,
                status = "Incompleto",
                website_name = website_name,
                website_url = website_url,
                website_description = website_description,
                website_justification = website_justification
            )
            db.session.add(project)
            db.session.flush()
            return project.id_project
        except Exception as e:
            raise e

    def update_status(id_project : int):
        try:
            project = Project.query.filter_by(id_project = id_project).first()
            project.status = "Completo"
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e
from app.models import db
from typing import Dict, List, Tuple
from app.models.question import Question
from app.models.question_x_subattribute_x_questionnaire_x_user_x_project import Question_X_Subattribute_X_Questionnaire_X_User_X_Project

class QuestionDataAccess:

    def create(description : str, statement_type : int):
        try:
            question : Question
            question = Question(
                description = description,
                statement_type = statement_type
            )
            db.session.add(question)
            db.session.flush()
            return question.id_question
        except Exception as e:
            raise e

    def list_questions(id_subattribute_x_questionnaire_x_user_x_project : int) -> List[Question]:
        try:
            return Question.query.filter(
                Question_X_Subattribute_X_Questionnaire_X_User_X_Project.id_subattribute_x_questionnaire_x_user_x_project == id_subattribute_x_questionnaire_x_user_x_project,
                Question.id_question == Question_X_Subattribute_X_Questionnaire_X_User_X_Project.id_question).all()
        except Exception as e:
            raise e

        
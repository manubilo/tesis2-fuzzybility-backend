from app.models import db
from app.models.role_x_user_x_project import Role_X_User_X_Project


class Role_X_User_X_ProjectDataAccess:

    def get_one(id_project: int, id_user: int):
        try:
            return Role_X_User_X_Project.query.filter(Role_X_User_X_Project.id_user == id_user,
                                                      Role_X_User_X_Project.id_project == id_project).first()
        except Exception as e:
            raise e

    def get_all(id_project: int, id_user: int):
        try:
            return Role_X_User_X_Project.query.filter(Role_X_User_X_Project.id_user == id_user,
                                                      Role_X_User_X_Project.id_project == id_project).all()
        except Exception as e:
            raise e

    def list(id_user: int):
        try:
            return Role_X_User_X_Project.query.filter(Role_X_User_X_Project.id_user == id_user).all()
        except Exception as e:
            raise e

    def create(id_user: int, id_role: int, id_project: int):
        try:
            role_x_user_x_project: Role_X_User_X_Project
            role_x_user_x_project = Role_X_User_X_Project(
                id_role=id_role,
                id_user=id_user,
                id_project=id_project
            )
            db.session.add(role_x_user_x_project)
            db.session.flush()
        except Exception as e:
            raise e

    def get_executor_user(id_project: int, id_user: int):
        try:
            return Role_X_User_X_Project.query.filter(Role_X_User_X_Project.id_user == id_user,
                                                      Role_X_User_X_Project.id_project == id_project,
                                                      Role_X_User_X_Project.id_role == 2).first()
        except Exception as e:
            raise e

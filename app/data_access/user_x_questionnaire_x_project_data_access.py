from app.models import db
from app.models.user_x_questionnaire_x_project import User_X_Questionnaire_X_Project
from app.models.role_x_user_x_project import Role_X_User_X_Project


class User_X_Questionnaire_X_ProjectDataAccess:

    def get_one(id_user: int, id_questionnaire: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter_by(id_user=id_user, id_questionnaire=id_questionnaire).first()

        except Exception as e:
            raise e

    def get_all(id_questionnaire: int, id_project: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter(User_X_Questionnaire_X_Project.id_questionnaire == id_questionnaire,
                                                               User_X_Questionnaire_X_Project.id_project == id_project).all()

        except Exception as e:
            raise e

    def get_all_questionnaires_by_project_and_user(id_project: int, id_user: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter_by(id_project=id_project,
                                                                  id_user=id_user).all()
        except Exception as e:
            raise e

    def get_all_executor_users(id_questionnaire: int, id_project: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter(
                Role_X_User_X_Project.id_project == id_project,
                Role_X_User_X_Project.id_role == 2).all()

        except Exception as e:
            raise e

    def get_last_records(id_user: int, id_questionnaire: int, id_project: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter_by(id_user=id_user, id_questionnaire=id_questionnaire, id_project=id_project).all()

        except Exception as e:
            raise e

    def get_last_record_one(id_user: int, id_questionnaire: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter_by(id_user=id_user, id_questionnaire=id_questionnaire).all()

        except Exception as e:
            raise e

    def get_one_with_project(id_project: int, id_user: int, id_questionnaire: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter_by(id_user=id_user, id_questionnaire=id_questionnaire, id_project=id_project).first()
        except Exception as e:
            raise e

    def get_users_by_questionnaire_by_project(id_questionnaire: int, id_project: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter_by(id_questionnaire=id_questionnaire, id_project=id_project).all()
        except Exception as e:
            raise e

    def create(id_user: int, id_project: int, id_questionnaire: int):
        try:
            user_x_questionnaire_x_project: User_X_Questionnaire_X_Project
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project(
                id_user=id_user,
                id_questionnaire=id_questionnaire,
                id_project=id_project
            )
            db.session.add(user_x_questionnaire_x_project)
            db.session.flush()

            return user_x_questionnaire_x_project.id_user_x_questionnaire_x_project
        except Exception as e:
            raise e

    def update(id_project: int, id_user_x_questionnaire_x_project: int):
        try:
            user_x_questionnaire_x_project: User_X_Questionnaire_X_Project
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project=id_user_x_questionnaire_x_project).first()
            user_x_questionnaire_x_project.id_project = id_project
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def update_flag(id_user_x_questionnaire_x_project: int):
        try:
            user_x_questionnaire_x_project: User_X_Questionnaire_X_Project
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project=id_user_x_questionnaire_x_project).first()
            user_x_questionnaire_x_project.flg_questionnaire_completed = 1
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def update_evaluation_date(id_user_x_questionnaire_x_project: int,  evaluation_date):
        try:
            user_x_questionnaire_x_project: User_X_Questionnaire_X_Project
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project=id_user_x_questionnaire_x_project).first()
            user_x_questionnaire_x_project.evaluation_date = evaluation_date
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def update_score(id_user: int, id_project: int, score: int):
        try:
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user=id_user, id_project=id_project).first()
            user_x_questionnaire_x_project.score = score
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def update_status(id_user: int, id_project: int, status: str):
        try:
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user=id_user, id_project=id_project).first()
            user_x_questionnaire_x_project.status = status
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def get_project_users_not_completed(id_questionnaire: int, id_project: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter(
                User_X_Questionnaire_X_Project.id_project == id_project,
                User_X_Questionnaire_X_Project.id_questionnaire == id_questionnaire,
                User_X_Questionnaire_X_Project.flg_questionnaire_completed == None).all()
        except Exception as e:
            raise e

    def check_project_questionnaires_completed(id_project: int):
        try:
            return User_X_Questionnaire_X_Project.query.filter(
                User_X_Questionnaire_X_Project.id_project == id_project,
                User_X_Questionnaire_X_Project.status != "Completo").all()
        except Exception as e:
            raise e

    def update_score_comment(id_user_x_questionnaire_x_project: int, score_comment: str):
        try:
            print("entre a update")
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project=id_user_x_questionnaire_x_project).first()
            user_x_questionnaire_x_project.score_comment = score_comment
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def update_improvement_comment(id_user_x_questionnaire_x_project: int, improvement_comment: str):
        try:
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project=id_user_x_questionnaire_x_project).first()
            user_x_questionnaire_x_project.improvement_comment = improvement_comment
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

    def update_general_comment(id_user_x_questionnaire_x_project: int, general_comment: str):
        try:
            user_x_questionnaire_x_project = User_X_Questionnaire_X_Project.query.filter_by(
                id_user_x_questionnaire_x_project=id_user_x_questionnaire_x_project).first()
            user_x_questionnaire_x_project.general_comment = general_comment
            db.session.commit()
            db.session.flush()
        except Exception as e:
            raise e

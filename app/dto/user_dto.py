from dataclasses import dataclass
from typing import Dict, List
from datetime import datetime
from app.models.user import User

@dataclass
class UserDTO():
    id_user : int = None
    username : str = None
    password : str = None
    first_name : str = None
    last_name : str = None
    email : str = None
    birth_date : datetime = None
    sex : str = None
    highest_education : str = None
    occupation : str = None
    creation_date : datetime = None
    active_status : bool = None


    def to_json(self) -> Dict:
        d = {}
        for key, value in self.__dict__.items():
            if key == 'birth_date' or key == 'creation_date':
                d[key] = value.__str__()
            else:
                d[key] = value
        return d

    @classmethod
    def from_model(cls, user : User):
        d = cls(
            id_user = user.id_user,
            username = user.username,
            password = user.password,
            first_name = user.first_name,
            last_name = user.last_name,
            email = user.email,
            birth_date = user.birth_date,
            sex = user.sex,
            highest_education = user.highest_education,
            occupation = user.occupation,
            creation_date = user.creation_date,
            active_status = user.active_status
        )
        return d
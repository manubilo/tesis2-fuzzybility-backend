from dataclasses import dataclass
from typing import Dict, List
from datetime import datetime
from app.models.question import Question


@dataclass
class QuestionDTO():
    id_question: int = None
    description: str = None
    statement_type: bool = None

    def to_json(self) -> Dict:
        d = {}
        for key, value in self.__dict__.items():
            d[key] = value
        return d

    @classmethod
    def from_model(cls, question: Question):
        d = cls(
            id_question=question.id_question,
            description=question.description,
            statement_type=question.statement_type,
        )
        return d

from dataclasses import dataclass
from typing import Dict, List
from datetime import datetime
from app.models.subattribute import Subattribute

@dataclass
class SubattributeDTO():
    id_subattribute : int = None
    name : str = None


    def to_json(self) -> Dict:
        d = {}
        for key, value in self.__dict__.items():
            d[key] = value
        return d

    @classmethod
    def from_model(cls, subattribute : Subattribute):
        d = cls(
            id_subattribute = subattribute.id_subattribute,
            name = subattribute.name
        )
        return d
from dataclasses import dataclass
from typing import Dict, List
from datetime import datetime
from app.models.project import Project

@dataclass
class ProjectDTO():
    id_project : int = None
    title : str = None
    creation_date : datetime = None
    status : str = None
    website_name : str = None
    website_url : str = None
    website_description : str = None
    website_justification : str = None

    def to_json(self) -> Dict:
        d = {}
        for key, value in self.__dict__.items():
            if key == 'creation_date':
                d[key] = value.__str__()
            else:
                d[key] = value
        return d

    @classmethod
    def from_model(cls, project : Project):
        d = cls(
            id_project = project.id_project,
            title = project.title,
            creation_date = project.creation_date,
            status = project.status,
            website_name = project.website_name,
            website_url = project.website_url,
            website_description = project.website_description,
            website_justification = project.website_justification
        )
        return d
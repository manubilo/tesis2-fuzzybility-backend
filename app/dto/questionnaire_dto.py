from dataclasses import dataclass
from typing import Dict, List
from datetime import datetime
from app.models.questionnaire import Questionnaire

@dataclass
class QuestionnaireDTO():
    id_questionnaire : int = None
    name : str = None
    creation_date : datetime = None
    likert_number : int = None

    def to_json(self) -> Dict:
        d = {}
        for key, value in self.__dict__.items():
            if key == 'creation_date':
                d[key] = value.__str__()
            else:
                d[key] = value
        return d

    @classmethod
    def from_model(cls, questionnaire : Questionnaire):
        d = cls(
            id_questionnaire = questionnaire.id_questionnaire,
            name = questionnaire.name,
            creation_date = questionnaire.creation_date,
            likert_number = questionnaire.likert_number
        )
        return d
import copy


class FuzzyLogicModule:

    def get_score(answers_array):

        num_usu = len(answers_array)
        num_sub = 0
        cant_items = []
        for i in range(len(answers_array)):
            for j in range(len(answers_array[i])):
                num_sub = num_sub + 1
            cant_items.append(len(answers_array[i]))

        # Fuzzyfication

        fuzzyficated_answers = []
        for i in range(len(answers_array)):
            row1 = []
            for j in range(len(answers_array[i])):
                row2 = []
                for k in range(len(answers_array[i][j])):
                    if answers_array[i][j][k] == 1:
                        row2.append(0)
                    elif answers_array[i][j][k] == 2:
                        row2.append(0.25)
                    elif answers_array[i][j][k] == 3:
                        row2.append(0.50)
                    elif answers_array[i][j][k] == 4:
                        row2.append(0.75)
                    elif answers_array[i][j][k] == 5:
                        row2.append(1)
                row1.append(row2)
            fuzzyficated_answers.append(row1)

        # Inference

        inference_list = []
        min_list = []
        max_list = []
        prom_list = []
        cant_items_len = 0

        num_usu = len(fuzzyficated_answers)

        min_list = copy.deepcopy(fuzzyficated_answers[0])
        max_list = copy.deepcopy(fuzzyficated_answers[0])
        for i in range(len(fuzzyficated_answers)):
            for j in range(len(fuzzyficated_answers[i])):
                for k in range(len(fuzzyficated_answers[i][j])):
                    min_list[j][k] = min(
                        min_list[j][k], fuzzyficated_answers[i][j][k])
                    max_list[j][k] = max(
                        max_list[j][k], fuzzyficated_answers[i][j][k])

        sum_aux = copy.deepcopy(fuzzyficated_answers[0])
        for i in range(1, len(fuzzyficated_answers)):
            for j in range(len(fuzzyficated_answers[i])):
                for k in range(len(fuzzyficated_answers[i][j])):
                    sum_aux[j][k] = sum_aux[j][k] + \
                        fuzzyficated_answers[i][j][k]

        for j in range(len(sum_aux)):
            for k in range(len(sum_aux[j])):
                sum_aux[j][k] = sum_aux[j][k] / len(fuzzyficated_answers)

        prom_list = sum_aux[:]

        # Convirtiendo promedios

        new_prom_list = []
        for i in range(len(prom_list)):
            new_row_prom = []
            for j in range(len(prom_list[i])):
                if prom_list[i][j] <= 0.25:
                    if 0.25 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0)
                    else:
                        new_row_prom.append(0.25)
                elif prom_list[i][j] > 0.25 and prom_list[i][j] <= 0.50:
                    if 0.50 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0.25)
                    else:
                        new_row_prom.append(0.50)
                elif prom_list[i][j] > 0.50 and prom_list[i][j] <= 0.75:
                    if 0.75 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0.50)
                    else:
                        new_row_prom.append(0.75)
                elif prom_list[i][j] > 0.75 and prom_list[i][j] <= 1:
                    if 1 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0.75)
                    else:
                        new_row_prom.append(1)
            new_prom_list.append(new_row_prom)

        # Defuzzification

        subfactor_list = []
        for i in range(len(min_list)):
            min_list_curr = min_list[i]
            max_list_curr = max_list[i]
            prom_list_curr = new_prom_list[i]
            size = len(min_list_curr)
            defuzz_row = []
            it = 0
            for j in range(len(min_list_curr)):
                subfactor = 0.25 * \
                    min_list_curr[j] + 0.5 * \
                    prom_list_curr[j] + 0.25 * max_list_curr[j]
                subfactor = round(subfactor, 2)
                defuzz_row.append(subfactor)
                it = it + 1
            subfactor_list.append(defuzz_row)

        factor_list = []
        for i in range(len(subfactor_list)):
            sum = 0
            for j in range(len(subfactor_list[i])):
                sum = sum + subfactor_list[i][j]
            prom = sum / len(subfactor_list[i])
            factor_list.append(prom)

        sum = 0
        for i in range(len(factor_list)):
            sum = sum + factor_list[i]

        usability = round(sum / len(factor_list), 2)

        return usability * 100

    def get_subfactor_score(answers_array):
        print("answers_array", answers_array)

        num_usu = len(answers_array)
        num_sub = 0
        cant_items = []
        for i in range(len(answers_array)):
            for j in range(len(answers_array[i])):
                num_sub = num_sub + 1
            cant_items.append(len(answers_array[i]))

        # Fuzzyfication

        fuzzyficated_answers = []
        for i in range(len(answers_array)):
            row1 = []
            for j in range(len(answers_array[i])):
                row2 = []
                for k in range(len(answers_array[i][j])):
                    if answers_array[i][j][k] == 1:
                        row2.append(0)
                    elif answers_array[i][j][k] == 2:
                        row2.append(0.25)
                    elif answers_array[i][j][k] == 3:
                        row2.append(0.50)
                    elif answers_array[i][j][k] == 4:
                        row2.append(0.75)
                    elif answers_array[i][j][k] == 5:
                        row2.append(1)
                row1.append(row2)
            fuzzyficated_answers.append(row1)

        # Inference

        inference_list = []
        min_list = []
        max_list = []
        prom_list = []
        cant_items_len = 0

        num_usu = len(fuzzyficated_answers)

        min_list = copy.deepcopy(fuzzyficated_answers[0])
        max_list = copy.deepcopy(fuzzyficated_answers[0])
        for i in range(len(fuzzyficated_answers)):
            for j in range(len(fuzzyficated_answers[i])):
                for k in range(len(fuzzyficated_answers[i][j])):
                    min_list[j][k] = min(
                        min_list[j][k], fuzzyficated_answers[i][j][k])
                    max_list[j][k] = max(
                        max_list[j][k], fuzzyficated_answers[i][j][k])

        sum_aux = copy.deepcopy(fuzzyficated_answers[0])
        for i in range(1, len(fuzzyficated_answers)):
            for j in range(len(fuzzyficated_answers[i])):
                for k in range(len(fuzzyficated_answers[i][j])):
                    sum_aux[j][k] = sum_aux[j][k] + \
                        fuzzyficated_answers[i][j][k]

        for j in range(len(sum_aux)):
            for k in range(len(sum_aux[j])):
                sum_aux[j][k] = sum_aux[j][k] / len(fuzzyficated_answers)

        prom_list = sum_aux[:]

        # Convirtiendo promedios

        new_prom_list = []
        for i in range(len(prom_list)):
            new_row_prom = []
            for j in range(len(prom_list[i])):
                if prom_list[i][j] <= 0.25:
                    if 0.25 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0)
                    else:
                        new_row_prom.append(0.25)
                elif prom_list[i][j] > 0.25 and prom_list[i][j] <= 0.50:
                    if 0.50 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0.25)
                    else:
                        new_row_prom.append(0.50)
                elif prom_list[i][j] > 0.50 and prom_list[i][j] <= 0.75:
                    if 0.75 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0.50)
                    else:
                        new_row_prom.append(0.75)
                elif prom_list[i][j] > 0.75 and prom_list[i][j] <= 1:
                    if 1 - prom_list[i][j] > 0.125:
                        new_row_prom.append(0.75)
                    else:
                        new_row_prom.append(1)
            new_prom_list.append(new_row_prom)

        # Defuzzification

        subfactor_list = []
        for i in range(len(min_list)):
            min_list_curr = min_list[i]
            max_list_curr = max_list[i]
            prom_list_curr = new_prom_list[i]
            size = len(min_list_curr)
            defuzz_row = []
            it = 0
            for j in range(len(min_list_curr)):
                subfactor = 0.25 * \
                    min_list_curr[j] + 0.5 * \
                    prom_list_curr[j] + 0.25 * max_list_curr[j]
                subfactor = round(subfactor, 2)
                defuzz_row.append(subfactor)
                it = it + 1
            subfactor_list.append(defuzz_row)

        print("subfactor_list", subfactor_list)
        return subfactor_list

    def get_factor_score(subfactor_list):
        print("subfactor_list en get_factor_score", subfactor_list)

        factor_list = []
        for i in range(len(subfactor_list)):
            sum = 0
            for j in range(len(subfactor_list[i])):
                sum = sum + subfactor_list[i][j]
            prom = sum / len(subfactor_list[i])
            factor_list.append(prom)

        print("factor_list", factor_list)

        return factor_list

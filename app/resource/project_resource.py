from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.project_controller import ProjectController


class ProjectResourceCreate(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data['id_user']
            print("data")
            #project_data = data['project_data']
            title = data["title"]
            website_name = data["website_name"]
            website_url = data["website_url"]
            website_description = data["website_description"]
            website_justification = data["website_justification"]
            participants_list = data["participants_list"]
            questionnaires_list = data["questionnaires_list"]
            print("participants list = ", participants_list)
            rpta = ProjectController.create(id_user, title,
                                            website_name, website_url, website_description, website_justification,
                                            participants_list, questionnaires_list)
            return rpta.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError("The project could not be created")


class ProjectResourceList(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data['id_user']
            print("id_user = ", id_user)
            answer = ProjectController.list(id_user)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError(
                'The required projects could not be found', 'Error . {}'.format(str(e)))


class ProjectResourceListProjectsWithQuestionnaires(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data['id_user']
            id_project = data['id_project']
            answer = ProjectController.list_projects_with_questionnaires(
                id_user, id_project)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError(
                'The required projects could not be found', 'Error . {}'.format(str(e)))

from . import Rpta
from flask_restful import Resource
from flask import Flask, request

from app.controller.questionnaire_controller import QuestionnaireController


class QuestionnaireResourceCreate(Resource):
    def post(self):
        try:
            print("Creando cuestionario...")
            data = request.get_json()
            id_user = data["id_user"]
            name = data["name"]
            likert_number = data["likert_number"]
            subattribute_list = data["subattribute_list"]
            rpta = QuestionnaireController.create(id_user, name,
                                                  likert_number, subattribute_list)
            print("rpta", rpta.toJson())
            return rpta.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('The questionnaire could not be created',
                            'Error : {}'.format(str(e)))


class QuestionnaireResourceListQuestionnaires(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_project = data["id_project"]
            id_user = data["id_user"]
            answer = QuestionnaireController.list_questionnaires(
                id_project, id_user)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError(
                'No se pudieron obtener los cuestionarios', 'Error : {}'.format(str(e)))


class QuestionnaireResourceListQuestionnairesByUser(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data["id_user"]
            answer = QuestionnaireController.list_questionnaires_by_user(
                id_user)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError(
                'No se pudieron obtener los cuestionarios del usuario', 'Error : {}'.format(str(e)))


class QuestionnaireResourceListQuestions(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_project = data["id_project"]
            id_user = data["id_user"]
            id_questionnaire = data["id_questionnaire"]
            answer = QuestionnaireController.list_questions(
                id_project, id_user, id_questionnaire)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se pudieron obtener las preguntas',
                            'Error : {}'.format(str(e)))


class QuestionnaireResourceAnswer(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data["id_user"]
            print("id_user", id_user)
            id_questionnaire = data["id_questionnaire"]
            id_project = data["id_project"]
            subattribute_list = data["subattribute_list"]
            answer = QuestionnaireController.answer(
                id_user, id_questionnaire, id_project, subattribute_list)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se pudo responder el cuestionario',
                            'Error : {}'.format(str(e)))


class QuestionnaireResourceComment(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data["id_user"]
            id_questionnaire = data["id_questionnaire"]
            id_project = data["id_project"]
            score_comment = data["score_comment"]
            improvement_comment = data["improvement_comment"]
            general_comment = data["general_comment"]
            print("entre aqui")
            answer = QuestionnaireController.comment(
                id_user, id_questionnaire, id_project, score_comment, improvement_comment, general_comment)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se pudo registrar el comentario',
                            'Error : {}'.format(str(e)))


class QuestionnaireResourceGetAnswers(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_questionnaire = data["id_questionnaire"]
            id_project = data["id_project"]
            print("entre a getAnswers")
            answer = QuestionnaireController.get_answers(
                id_questionnaire, id_project)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se pudo obtener las respuestas',
                            'Error : {}'.format(str(e)))

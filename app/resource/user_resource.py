from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.user_controller import UserController


class UserResourceGetOne(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_user = data['id_user']
            answer = UserController.get_one(id_user)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se obtuvo correctamente el usuario', 'Error : {}'.format(str(e)))

class UserResourceGetAll(Resource):
    def post(self):
        try:
            data = request.get_json()
            answer = UserController.get_all()
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se pudo obtener correctamente la lista de usuarios', 'Error : {}'.format(str(e)))

class UserResourceLogin(Resource):
    def post(self):
        try:
            data = request.get_json()
            username = data['username']
            password = data['password']
            answer = UserController.login(username, password)
            return answer.toJson()
        except Exception as e:
            answer = Rpta()
            answer.setError('No se obtuvo correctamente el usuario', 'Error : {}'.format(str(e)))

class UserResourceCreate(Resource):
    def post(self):
        try:
            data = request.get_json()
            username = data['username']
            password = data['password']
            first_name = data['first_name']
            last_name = data['last_name']
            email = data['email']
            birth_date = data['birth_date']
            sex = data['sex']
            highest_education = data['highest_education']
            occupation = data['occupation']
            active_status = data['active_status']
            rpta = UserController.create_user(username, password, first_name, last_name, email,
                                                birth_date, sex, highest_education, occupation, active_status)
            return rpta.toJson()

        except Exception as e:
            answer = Rpta()
            answer.setError('The user was not created successfully', 'Error : {}'.format(str(e)))
            return answer.toJson()
